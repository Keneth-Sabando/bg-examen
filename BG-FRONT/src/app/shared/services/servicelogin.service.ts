import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class servicelogin {

  private URL = "https://api.escuelajs.co/api/v1/categories";

  constructor(public http: HttpClient) {

  }

  obtenerLogin( params?: any) {
    return this.http.get(this.URL)
  }
}
