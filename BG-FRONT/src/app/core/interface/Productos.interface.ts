export interface Productos {
  Id          : String;
  Descripcion : String;
  Precio      : String;
  Estado      : String;
  Detalle     : String;
  Imagen      : String;
}
