import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductosComponent } from './pages/productos/productos.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { PlanesComponent } from './pages/planes/planes.component';

const routes: Routes = [
  {
    path: 'productos',
    component: ProductosComponent
  },
  {
    path: 'producto',
    component: ProductoComponent
  },
  {
    path: 'planes',
    component: PlanesComponent
  },
  {
    path: 'producto/:id',
    component: ProductoComponent
  },
  {
    path: '',
    redirectTo: 'productos',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
