import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class ServiceProductsService {
  private URL = "https://api.escuelajs.co/api/v1/categories";

  constructor(public http: HttpClient) {

  }


  insertArchivo(data?: any) {
    return this.http.post(this.URL, data);
  }


  obtenerProductos( params?: any) {
    return this.http.get(this.URL)
  }
}
