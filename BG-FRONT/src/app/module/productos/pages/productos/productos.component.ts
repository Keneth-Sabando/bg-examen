import { Component } from '@angular/core';
import { ServiceProductsService } from '../../services/service-products.service';
import { Productos } from 'src/app/core/interface/Productos.interface';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent {
  products: Productos[] = [{
    Id: "",
    Descripcion: "",
    Precio: "",
    Estado: "",
    Detalle: "",
    Imagen: "",
  }];

  constructor(private _service: ServiceProductsService) { }


  ngOnInit(): void {
    this.obtenerCostosProyectosComposite();
    ;
  }


  private obtenerCostosProyectosComposite() {
    this._service.obtenerProductos().subscribe({
      next: (res: any) => {
        res.forEach((el: any) => {
          this.products.push(el)
          console.log(el)
        })
      },
      error: (error: any) => {
        console.error(error)
      }
    });
  }
}
