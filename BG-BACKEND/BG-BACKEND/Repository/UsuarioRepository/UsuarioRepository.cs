﻿using BG_BACKEND.Modelos.Producto;
using BG_BACKEND.Utils;
using System.Data.SqlClient;
using System.Data;
using BG_BACKEND.Modelos.Usuario;

namespace BG_BACKEND.Repository.UsuarioRepository
{
    public class UsuarioRepository
    {

        public LoginResponse login(LoginRequest loginRequest)
        {
            LoginResponse loginResponse = new LoginResponse();

            Conexion cn = new Conexion();

            using (var conexion = new SqlConnection(cn.getCadenaSQL()))
            {
                
                conexion.Open();

                SqlCommand cmd = new SqlCommand("spLogin", conexion);

                cmd.Parameters.AddWithValue("Username", loginRequest.Username);
                cmd.Parameters.AddWithValue("Contrasena", loginRequest.Contrasena);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        loginResponse.Success = Convert.ToInt32(dr["isValid"]);

                        /*loginResponse.Usuario.Nombre = dr["Estado"].ToString();
                        loginResponse.Usuario.Contrasena = dr["Estado"].ToString();
                        loginResponse.Usuario.Username = dr["Estado"].ToString();
                        loginResponse.Usuario.Telefono = dr["Estado"].ToString();
                        loginResponse.Usuario.Plan = dr["Plan"].ToString();*/

                    }
                }

                conexion.Close();

            }

            return loginResponse;
        }

    }
}
