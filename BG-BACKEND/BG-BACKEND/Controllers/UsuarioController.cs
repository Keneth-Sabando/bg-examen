﻿using BG_BACKEND.Modelos.Producto;
using BG_BACKEND.Modelos.Usuario;
using BG_BACKEND.Repository.UsuarioRepository;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BG_BACKEND.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {

        private readonly UsuarioRepository _repository = new UsuarioRepository();


        [HttpPost]
        [ProducesResponseType(typeof(LoginResponse), 200)]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            LoginResponse loginResponse = _repository.login(loginRequest);
            return Ok(loginResponse);
        }
    }
}
