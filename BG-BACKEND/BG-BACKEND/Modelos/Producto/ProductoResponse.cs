﻿namespace BG_BACKEND.Modelos.Producto
{
    public class ProductoResponse
    {
        public int Success { get; set; }
        public List<Producto> Productos { get; set; }
    }
}
