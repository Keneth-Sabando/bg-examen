﻿namespace BG_BACKEND.Modelos.Usuario
{
    public class LoginResponse
    {

        public int Success { get; set; }
        public Usuario Usuario { get; set; }

    }
}
